package main

import (
	"github.com/tidwall/gjson"
	"github.com/tidwall/sjson"
	"gitlab.com/k33g_org/side-projects/galago/galago-executor/api/function"
)

func main() {}

func oh(body string) string {
	firstName := gjson.Get(body, "FirstName")
	lastName := gjson.Get(body, "LastName")

	sum := 0
	for i := 0; i < 1000000; i++ {
		sum += i
	}
	
	result, _ := sjson.Set(`{"message":""}`, "message", "Oh "+firstName.Str+" "+lastName.Str+" "+string(rune(sum)))

	return result
}

//export handle
func handle(parameters *int32) *byte {
	return helpers.Use(oh, parameters)
}
