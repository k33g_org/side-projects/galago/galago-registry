#!/bin/bash
mkcert -install
mkcert galago.local localhost 127.0.0.1 ::1
cp galago.local+3-key.pem galago.local.key
cp galago.local+3.pem galago.local.crt
rm *.pem

