# Galago registry


## Upload wasm file

```bash
curl -v \
  -F "file=@oh.wasm" \
	-H "Content-Type: multipart/form-data" \
	-X POST https://localhost:9999/wasm/upload
```

## Delete wasm file

```bash
curl -v \
	-X DELETE https://localhost:9999/wasm/delete/oh.wasm
```

## Download wasm file

```bash
curl https://localhost:9999/wasm/download/oh.wasm --output oh.wasm
```

